FileInDir plugin 2.0.0.2 for Total Commander
============================================

 * License:
-----------

This software is released as freeware.


 * Disclaimer:
--------------

This software is provided "AS IS" without any warranty, either expressed or
implied, including, but not limited to, the implied warranties of
merchantability and fitness for a particular purpose. The author will not be
liable for any special, incidental, consequential or indirect damages due to
loss of data or any other reason.


 * Installation:
----------------

Open the plugin archive using Total Commander and installation will start.

The default configuration file fileindir.ini is initialized during the very
first plugin initialization. The configuration file must be named with the same
name as the plugin file. If a configuration file is found in the plugin directory,
it is utilized first instead of the default configuration file directory.


 * Description:
---------------

The plugin can be used to search for directories which contain specified files,
and optionally with specified text content.

Example configurations:
[HasDLL]
; match DLL extension
SearchFile=*.dll 

[HasConfigFile]
; match CFG extension
SearchFile=*.cfg 
; Only if it also contains (case-insensitive) the specified content
IgnoreCase=1
SearchContent=for_winapi

The plugin can also be used in the search dialog for a full text search.


 * ChangeLog:
-------------

 o Version 2.0.0.2 (07.07.2019)
   - minor code improvements
 o Version 2.0.0.1 (02.02.2019)
   - first public version


 * References:
--------------

 o FileInDir plugin 1.1
   - https://totalcmd.net/plugring/fileindir.html

 
 * Trademark and Copyright Statements:
--------------------------------------

 o Total Commander is Copyright © 1993-2019 by Christian Ghisler, Ghisler Software GmbH.
   - http://www.ghisler.com


 * Feedback:
------------

If you have problems, questions, suggestions please contact Thomas Beutlich.
 o Email: support@tbeu.de
 o URL: http://tbeu.totalcmd.net