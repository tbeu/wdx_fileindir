#include "stdafx.h"
#include "contplug.h" // WDX
#include "cunicode.h"
#include "resource.h"

#include <algorithm>
#include <cctype>
//#include <codecvt>
#include <fstream>
#include <iterator>
#include <optional>
#include <shared_mutex>
#include <sstream>
#include <string>
#include <string_view>
#include <tuple>
#include <strict_variant/variant.hpp>
#include <vector>

using TriBool = std::optional<bool>;
using FindFunc = std::vector<std::wstring>(*)(const std::wstring&, const std::wstring&);
using FindPredFunc = bool(*)(const WIN32_FIND_DATAW&);
using CmpPredFunc = bool(*)(wchar_t, wchar_t);
using SearchContFunc = TriBool(*)(const std::wstring&, const std::wstring&, const std::wstring&);
enum class FindKind { FINDALL, FINDANY };
enum FieldFields {       FIELDNAME,    FIELDTYPE, FIELDCASE,      FIELDFILE,    FIELDCONT,    FIELDFUNC };
using Field = std::tuple<std::wstring, int,       SearchContFunc, std::wstring, std::wstring, FindFunc>;
using Fields = std::vector<Field>;
using String = strict_variant::variant<std::wstring, std::string>;

static constexpr char detectString[]{ "SIZE=0" };
static constexpr wchar_t plugin[]{ L"FileInDir plugin 2.0.0.2" };
static HINSTANCE hinst{ nullptr };
static Fields fields{};
static String vFullText{};
static std::shared_timed_mutex fieldsMutex;
static std::mutex fullTextMutex;

static char* strlcpy(char* str1, const char* str2, int maxSize)
{
	if (int(std::strlen(str2)) >= maxSize - 1)
	{
		std::strncpy(str1, str2, maxSize - 1);
		str1[maxSize - 1] = 0;
	}
	else
	{
		std::strcpy(str1, str2);
	}
	return str1;
}

inline static bool isFile(const WIN32_FIND_DATAW& fd)
{
	return !(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY);
}

inline static bool isSubDir(const WIN32_FIND_DATAW& fd)
{
	return fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY && 0 != wcscmp(L".", fd.cFileName) && 0 != wcscmp(L"..", fd.cFileName);
}

template<FindKind find, FindPredFunc predicate>
static std::vector<std::wstring> getItemNamesWithinDirW(const std::wstring& folder, const std::wstring& fileSearch)
{
	std::vector<std::wstring> names{};
	std::wstring searchPath = folder + L"\\" + fileSearch;
	WIN32_FIND_DATAW fd;
	auto hFind = ::FindFirstFileW(searchPath.c_str(), &fd);
	if (INVALID_HANDLE_VALUE != hFind)
	{
		do {
			if (predicate(fd))
			{
				names.push_back(fd.cFileName);
				if (FindKind::FINDANY == find)
				{
					break;
				}
			}
		} while (::FindNextFileW(hFind, &fd));
		::FindClose(hFind);
	}
	return names;
}

constexpr auto getAnyFileNameWithinDirW = getItemNamesWithinDirW<FindKind::FINDANY, isFile>;
constexpr auto getAllFileNamesWithinDirW = getItemNamesWithinDirW<FindKind::FINDALL, isFile>;
constexpr auto getAllSubDirNamesWithinDirW = getItemNamesWithinDirW<FindKind::FINDALL, isSubDir>;

inline static std::wstring getAllItemNamesWithInDirW(FindFunc findFunc, const std::wstring& FileName, const std::wstring& fileSearch)
{
	const auto& files = findFunc(FileName, fileSearch);
	std::wostringstream ss;
	std::copy(files.cbegin(), files.cend(), std::ostream_iterator<std::wstring, wchar_t, std::char_traits<wchar_t>>(ss, L"\r\n"));
	return ss.str();
}

inline static bool cmpCharsIgnoreCase(wchar_t c1, wchar_t c2)
{
	return std::toupper(c1) == std::toupper(c2);
}

inline static bool cmpCharsCaseSensitive(wchar_t c1, wchar_t c2)
{
	return c1 == c2;
}

template<CmpPredFunc cmp>
static TriBool searchFileCont(const std::wstring& folder, const std::wstring& fileSearch, const std::wstring& fileCont)
{
	std::wifstream wifs;
	std::wstring searchPath = folder + L"\\" + fileSearch;
	wifs.open(searchPath);

	if (wifs.is_open())
	{
		bool found{ false };
		while (!wifs.eof() && !found)
		{
			std::wstring line;
			std::getline(wifs, line);
			found = line.cend() != std::search(line.cbegin(), line.cend(), fileCont.cbegin(), fileCont.cend(), cmp);
		}
		wifs.close();
		return TriBool(found);
	}
	else
	{
		return TriBool();
	}
}

constexpr auto searchFileContIgnoreCase = searchFileCont<cmpCharsIgnoreCase>;
constexpr auto searchFileContCaseSensitive = searchFileCont<cmpCharsCaseSensitive>;

static std::wstring getIniFileName(const ContentDefaultParamStruct* dps)
{
	std::wstring iniFileName{ L"" };
	std::wstring iniFileNameLookFirst{ L"" };
	wchar_t Path[wdirtypemax];
	if (::GetModuleFileNameW(hinst, Path, wdirtypemax) != 0)
	{
		char dd[_MAX_DRIVE];
		char pd[_MAX_PATH];
		wchar_t wdp[_MAX_DRIVE];
		wchar_t wpp[_MAX_PATH];
		wchar_t wfp[_MAX_FNAME];
		_splitpath_s(dps->DefaultIniName, dd, _MAX_DRIVE, pd, _MAX_PATH, nullptr, 0, nullptr, 0);
		_wsplitpath_s(Path, wdp, _MAX_DRIVE, wpp, _MAX_PATH, wfp, _MAX_FNAME, nullptr, 0);
		wchar_t wdd[_MAX_DRIVE];
		wchar_t wpd[_MAX_PATH];
		awlcopy(wdd, dd, _MAX_DRIVE);
		awlcopy(wpd, pd, _MAX_PATH);
		iniFileName = std::wstring(wdd) + std::wstring(wpd) + wfp + L".ini";
		iniFileNameLookFirst = std::wstring(wdp) + std::wstring(wpp) + wfp + L".ini";
	}

	// see if the INI file already exists in the plugin directory
	WIN32_FIND_DATAW findData;
	if (INVALID_HANDLE_VALUE != ::FindFirstFileW(iniFileNameLookFirst.c_str(), &findData))
	{
		iniFileName = iniFileNameLookFirst;
	}
	else
	{
		// see if the INI file already exists
		ZeroMemory(&findData, sizeof(WIN32_FIND_DATAW));
		if (INVALID_HANDLE_VALUE == ::FindFirstFileW(iniFileName.c_str(), &findData))
		{
			// load default INI file string from resource
			HRSRC hRes = ::FindResourceW((HMODULE)hinst, MAKEINTRESOURCEW(IDR_INIFILE), L"FILE");
			HGLOBAL hGlobal = ::LoadResource((HMODULE)hinst, hRes);
			DWORD dwBytesToWrite = ::SizeofResource((HMODULE)hinst, hRes);
			DWORD dwBytesWritten = 0;
			BOOL err = FALSE;
			auto iniStr = static_cast<const char*>(::LockResource(hGlobal));
			if (iniStr)
			{
				auto hFile = ::CreateFileW(iniFileName.c_str(), GENERIC_WRITE, 0, nullptr, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, nullptr);
				if (INVALID_HANDLE_VALUE == hFile)
				{
					DisplayLastErrorMsgW(L"CreateFileW", iniFileName.c_str(), MB_ICONWARNING | MB_OK);
				}
				else
				{
					while (dwBytesWritten < dwBytesToWrite)
					{
						if (FALSE == ::WriteFile(hFile, iniStr + dwBytesWritten, dwBytesToWrite - dwBytesWritten, &dwBytesWritten, nullptr))
						{
							DisplayLastErrorMsgW(L"WriteFile", iniFileName.c_str(), MB_ICONWARNING | MB_OK);
							err = TRUE;
							break;
						}
					}
					::CloseHandle(hFile);
				}
			}
			UnlockResource(hGlobal);
			::FreeResource(hRes);
			if (err == FALSE)
			{
				wchar_t msg[_MAX_PATH];
				_snwprintf(msg, _MAX_PATH - 1, L"New initialization file is \"%s\"", iniFileName.c_str());
				::MessageBoxW(nullptr, msg, plugin, MB_ICONINFORMATION | MB_OK);
			}
		}
	}
	return iniFileName;
}

static void fillFields(const std::wstring& iniFileName, const ContentDefaultParamStruct* dps)
{
	std::lock_guard<std::shared_timed_mutex> wLock(fieldsMutex);
	if (fields.empty())
	{
		// boolean search fields from INI file
		{
			std::vector<wchar_t> sections;
			size_t need{ 64 };
			do {
				sections.reserve(need);
				auto rc = ::GetPrivateProfileSectionNamesW(sections.data(), DWORD(need), iniFileName.c_str());
				if (rc == need - 2)
				{
					need *= 2;
				}
			} while (sections.capacity() < need);

			auto fieldName = sections.data();
			while (L'\0' != *fieldName)
			{
				auto searchContFunc = 0 == ::GetPrivateProfileIntW(fieldName, L"IgnoreCase", 1, iniFileName.c_str()) ? searchFileContCaseSensitive : searchFileContIgnoreCase;
				std::vector<wchar_t> searchFile;
				searchFile.reserve(_MAX_PATH);
				::GetPrivateProfileStringW(fieldName, L"SearchFile", L"*.*", searchFile.data(), _MAX_PATH, iniFileName.c_str());
				std::vector<wchar_t> searchCont;
				searchCont.reserve(_MAX_PATH);
				::GetPrivateProfileStringW(fieldName, L"SearchContent", L"", searchCont.data(), _MAX_PATH, iniFileName.c_str());
				auto findFunc = L'\0' == *searchCont.data() ? getAnyFileNameWithinDirW : getAllFileNamesWithinDirW;
				fields.emplace_back(std::make_tuple(fieldName, ft_boolean, searchContFunc, searchFile.data(), searchCont.data(), findFunc));
				fieldName += std::wcslen(fieldName) + 1;
			}
		}

		// two trailing full text fields
		int fieldType = ((dps->PluginInterfaceVersionHi == 2 && dps->PluginInterfaceVersionLow >= 11) || dps->PluginInterfaceVersionHi > 2) ? ft_fulltextw : ft_fulltext;
		fields.emplace_back(std::make_tuple(L"DirFiles", fieldType, nullptr, L"*.*", L"", getAllFileNamesWithinDirW));
		fields.emplace_back(std::make_tuple(L"DirSubdirs", fieldType, nullptr, L"*.*", L"", getAllSubDirNamesWithinDirW));
	}
}

template<typename T, typename StringCopyFunc, StringCopyFunc copyFunc>
static int handleFullTextT(const std::wstring& FileName, int UnitIndex, void* FieldValue, int maxSize, int fieldType, FindFunc findFunc, const std::wstring& fileSearch)
{
	using StringType = std::basic_string<T, std::char_traits<T>, std::allocator<T>>;
	using StringViewType = std::basic_string_view<T>;

	std::lock_guard<std::mutex> lock(fullTextMutex);
	if (UnitIndex == 0)
	{
		vFullText = getAllItemNamesWithInDirW(findFunc, FileName, fileSearch);
		if constexpr (std::is_same_v<T, char>)
		{
			//std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
			//vFullText = converter.to_bytes(strict_variant::get<std::wstring>(&vFullText)->c_str());
			const auto& fullText = strict_variant::get<std::wstring>(&vFullText);
			std::vector<T> buf{};
			buf.reserve(fullText->size() + 1);
			vFullText = StringType(walcopy(buf.data(), fullText->c_str(), int(buf.capacity())));
		}
	}
	auto fullText = strict_variant::get<StringType>(&vFullText);
	if (!fullText || fullText->empty())
	{
		return ft_fieldempty;
	}
	constexpr unsigned s = sizeof(T);
	if (UnitIndex == -1 || int(fullText->size()) <= UnitIndex / s)
	{
		fullText->clear();
		return ft_fieldempty;
	}
	if (UnitIndex >= 0)
	{
		StringViewType view{ fullText->c_str(), fullText->size() };
		copyFunc(static_cast<StringType::pointer>(FieldValue), view.substr(UnitIndex / s).data(), maxSize / s);
	}
	return fieldType;
}

template<typename T>
using StringCopyFunc = T*(*)(T*, const T*, int);

constexpr auto handleFullText = handleFullTextT<char, StringCopyFunc<char>, strlcpy>;
constexpr auto handleFullTextW = handleFullTextT<wchar_t, StringCopyFunc<wchar_t>, wcslcpy>;

void __stdcall ContentPluginUnloading(void)
{
	std::lock_guard<std::shared_timed_mutex> wLock(fieldsMutex);
	fields.clear();
}

int __stdcall ContentGetSupportedField(int FieldIndex, char* FieldName, char* Units, int maxSize)
{
	std::shared_lock<std::shared_timed_mutex> rLock(fieldsMutex);
	if (FieldIndex < 0 || FieldIndex >= int(fields.size()))
	{
		return ft_nomorefields;
	}

	const auto& field = fields.at(FieldIndex);
	{
		//std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
		//strlcpy(FieldName, converter.to_bytes(std::get<FIELDNAME>(field).c_str()).c_str(), maxSize);
		const auto& fieldName = std::get<FIELDNAME>(field);
		walcopy(FieldName, fieldName.c_str(), maxSize);
	}
	strlcpy(Units, "", maxSize);
	const auto fieldType = std::get<FIELDTYPE>(field);
	return ft_fulltextw == fieldType ? ft_fulltext : fieldType;
}

int __stdcall ContentGetValue(const char* FileName, int FieldIndex, int UnitIndex, void* FieldValue, int maxSize, int flags)
{
	std::vector<wchar_t> FileNameW{};
	FileNameW.reserve(std::strlen(FileName) + 1);
	const auto rc = ContentGetValueW(awlcopy(FileNameW.data(), FileName, int(FileNameW.capacity())), FieldIndex, UnitIndex, FieldValue, maxSize, flags);
/*
	if (ft_numeric_floating == rc && maxSize > sizeof(double))
	{
		auto p = _wcsdup((const wchar_t*)(static_cast<char*>(FieldValue) + sizeof(double)));
		walcopy(static_cast<char*>(FieldValue) + sizeof(double), p, maxSize - sizeof(double));
		free(p);
	}
*/
	return rc;
}

int __stdcall ContentGetValueW(const wchar_t* FileName, int FieldIndex, int UnitIndex, void* FieldValue, int maxSize, int flags)
{
	if (flags & CONTENT_DELAYIFSLOW)
	{
		return ft_delayed;
	}
	std::shared_lock<std::shared_timed_mutex> rLock(fieldsMutex);
	if (FieldIndex < 0 || FieldIndex >= int(fields.size()))
	{
		return ft_nosuchfield;
	}

	const auto& field = fields.at(FieldIndex);
	const auto fieldType = std::get<FIELDTYPE>(field);
	const auto& fieldFile = std::get<FIELDFILE>(field);
	const auto& findFunc = std::get<FIELDFUNC>(field);
	if (ft_boolean == fieldType)
	{
		auto found = static_cast<int*>(FieldValue);
		const auto& files = findFunc(FileName, fieldFile);
		const auto& fieldCont = std::get<FIELDCONT>(field);
		if (fieldCont.empty())
		{
			*found = int(!files.empty());
		}
		else
		{
			*found = 0;
			const auto& searchFileContFunc = std::get<FIELDCASE>(field);
			for (auto& file : files)
			{
				auto foundContent = searchFileContFunc(FileName, file, fieldCont);
				if (foundContent)
				{
					if (foundContent.value())
					{
						*found = 1;
						break;
					}
				}
				else
				{
					return ft_fileerror;
				}
			}
		}
	}
	else if (ft_fulltextw == fieldType)
	{
		return handleFullTextW(FileName, UnitIndex, FieldValue, maxSize, ft_fulltextw, findFunc, fieldFile);
	}
	else if (ft_fulltext == fieldType)
	{
		return handleFullText(FileName, UnitIndex, FieldValue, maxSize, ft_fulltext, findFunc, fieldFile);
	}

	return fieldType; // very important!
}

void __stdcall ContentSetDefaultParams(ContentDefaultParamStruct* dps)
{
	if (nullptr == dps)
	{
		return;
	}

	const auto iniFileName{ getIniFileName(dps) };
	fillFields(iniFileName, dps);
}

int __stdcall ContentGetDetectString(char* DetectString, int maxLen)
{
	strlcpy(DetectString, detectString, maxLen);
	return 0;
}

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
		{
			hinst = hinstDLL;
			::DisableThreadLibraryCalls(hinst);
			break;
		}

		case DLL_PROCESS_DETACH:
			hinst = nullptr;
			break;
	}
	return TRUE;
}
